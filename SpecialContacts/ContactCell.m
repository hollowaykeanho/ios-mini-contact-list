//
//  ContactCell.m
//  SpecialContacts
//
//  Created by Kean Ho Chew on 06/04/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
