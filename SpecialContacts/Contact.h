//
//  Contact.h
//  SpecialContacts
//
//  Created by Kean Ho Chew on 06/04/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property NSString *name;
@property NSString *phoneNumber;

@end
