//
//  ContactCell.h
//  SpecialContacts
//
//  Created by Kean Ho Chew on 06/04/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell

@property IBOutlet UILabel *nameLabel;
@property IBOutlet UILabel *phoneLabel;

@end
