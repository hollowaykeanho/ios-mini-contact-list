//
//  ViewController.m
//  SpecialContacts
//
//  Created by Kean Ho Chew on 06/04/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import "ViewController.h"
#import "Contact.h"
#import "ContactCell.h"


@interface ViewController () <UIGestureRecognizerDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;

@end

@implementation ViewController
{
    NSMutableArray *contacts;
}

- (void)initialize
{
    contacts = [[NSMutableArray alloc] initWithCapacity:50];
    
    Contact *john = [[Contact alloc] init];
    john.name = @"John";
    john.phoneNumber = @"+6014332337";
    [contacts addObject:john];
    
    Contact *selina = [[Contact alloc] init];
    selina.name = @"Selina";
    selina.phoneNumber = @"+60124443372";
    [contacts addObject:selina];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Drag and Drop
- (IBAction)didPressedLong:(id)sender
{
    static UIView *snapshot = nil;
    static NSIndexPath *sourceIndexPath = nil;
    CGPoint center;
    ContactCell *cell;
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    switch (state) {
        case UIGestureRecognizerStateBegan:
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                
                snapshot = [self customSnapshotFromView:cell];
                
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self.tableView addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    
                    cell.alpha = 0.0;
                } completion:^(BOOL finished) {
                    
                    cell.hidden = YES;
                }];
                
            }
            break;
        case UIGestureRecognizerStateChanged:
            center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                
                [contacts exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                
                sourceIndexPath = indexPath;
            }
            break;
        default:
            cell = [self.tableView cellForRowAtIndexPath:sourceIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                
                cell.alpha = 1.0;
                
            }completion:^(BOOL finished) {
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
            }];
            break;
    }
}

- (UIView *)customSnapshotFromView:(UIView *)inputView {
    
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

# pragma mark - Gesture
- (IBAction)didSwipeLeft:(id)sender
{
    UISwipeGestureRecognizer *swipeGestureRecognizer = sender;
    CGPoint location = [swipeGestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    if(indexPath) {
        ContactCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        cell.nameLabel.textColor = [UIColor blackColor];
        cell.phoneLabel.textColor = [UIColor blackColor];
    }
    
}

- (IBAction)didSwipeRight:(id)sender
{
    UISwipeGestureRecognizer *swipeGestureRecognizer = sender;
    CGPoint location = [swipeGestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    if(indexPath) {
        ContactCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        cell.nameLabel.textColor = [UIColor redColor];
        cell.phoneLabel.textColor = [UIColor redColor];
    }
}

# pragma mark - AlertController IBAction
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(nonnull NSIndexPath *)indexPath
{
    Contact *contact = contacts[indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Edit Contact"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";
        textField.text = contact.name;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Number";
        textField.text = contact.phoneNumber;
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Update"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                contact.name = ((UITextField *)alert.textFields[0]).text;
                                                contact.phoneNumber = ((UITextField *)alert.textFields[1]).text;
                                                [self.tableView reloadData];
                                            }]
     ];

    [alert addAction:[UIAlertAction actionWithTitle:@"Delete"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action){
                                                [contacts removeObject:contact];
                                                [self.tableView reloadData];
                                                
                                            }]
     ];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                            handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)didPressedAddContactButton:(id)sender
{
    if (!self.tableView.isEditing) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add New Contact"
                                                                       message:@"Name : Phone"
                                                                preferredStyle:UIAlertControllerStyleAlert
                                    ];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"Name";
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"Number";
        }];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Add"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action){
                                                    Contact *contact = [[Contact alloc] init];
                                                    
                                                    contact.name = ((UITextField *)alert.textFields[0]).text;
                                                    contact.phoneNumber = ((UITextField *)alert.textFields[1]).text;
                                                    [contacts addObject:contact];
                                                    [self.tableView reloadData];
                                                }]
         ];
        

        
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                  style:UIAlertActionStyleCancel
                                                handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - edit mode
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(nonnull NSIndexPath *)sourceIndexPath
      toIndexPath:(nonnull NSIndexPath *)destinationIndexPath
{
    id contact = contacts[sourceIndexPath.row];
    [contacts removeObjectAtIndex:sourceIndexPath.row];
    [contacts insertObject:contact atIndex:destinationIndexPath.row];
    
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return contacts.count;
}

#pragma mark - Table view cell source
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contact"];
    
    Contact *contact = contacts[indexPath.row];
    
    cell.nameLabel.text = contact.name;
    cell.phoneLabel.text = contact.phoneNumber;
    
    return cell;
}



@end
